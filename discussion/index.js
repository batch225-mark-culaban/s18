console.log("hello");

//Functions Parameters and Arguments

//"name" is called a parameter that acts as a named/ variable/ container that exist only inside the function



// "Juana" - argument- is is the information/data provided directly into the function


function printInput(name){
	console.log("My name is " + name);

} ;

printInput("Nehemiah");


printInput("mark");

let samplevariable = "Yui";

printInput(samplevariable);


function checkDivisibilityBy8(num){
				let remainder = num % 8;
				console.log("The remainder of " + num + " divided by 8 is: " + remainder);
				let isDivisibleBy8 = remainder === 0;
				console.log("Is " + num + " divisible by 8?");
				console.log(isDivisibleBy8);
			}

			checkDivisibilityBy8(64);
			checkDivisibilityBy8(28);


//Function as Arguments
// Funcion parameters can also accept other functions as arguments.

/*

function argumentFunction(){
	console.log("This function was passes as an argument before the message was printed");

}

function invokeFunction(argumentFunction){
	argumentFunction();
	console.log("This code comes from invokeFunction");

}

invokeFunction(argumentFunction);
*/



//Multiple Arguments - will correspond to the number of paremeters declared in a function in suceeding order.

function createFullName(firstName, middleName, lastName) {
	console.log(firstName + ` ` + middleName + ` ` + lastName);

}


createFullName(`Juan`, `Dela`);
createFullName(`Juan`, `Dela`, `Cruz`);
createFullName(`Juan`, `Dela`, `Cruz, Hello`);

let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

// The Return Statements allowws us to output a value from a function to be passed to the line/block of code that invoked/called the function.

//The Returnnnnnn statement also stops the execution of the function and any code after the return statement will be not be executed.

function returnFullName(firstName, middleName, lastName){

	return firstName + ' ' + middleName + ' ' + lastName; //dapat before console.log, kung after dli mu print

	console.log("This message will not be printed");
	//return firstName + ' ' + middleName + ' ' + lastName;  (dli mu print)
}


//returnFullName('Joe', 'Jayson', 'Helberg');

let completeName = returnFullName('Joe', 'Jayson', 'Helberg');
console.log(completeName);



function returnAddress(city, country){
	let fullAddress = city + ", " + country;
	return fullAddress; 

}

let myAddress = returnAddress("Cebu City", "Philippines");
console.log(myAddress);
































